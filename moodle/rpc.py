from dataclasses import dataclass
from typing import Literal, overload
import requests

from moodle.types.webservice import SiteInfo


@dataclass
class MoodleException(Exception):
  exception: str
  errorcode: str
  message: str
  debuginfo: str | None = None


class MoodleRPC:
  """
  Wrappers for
  https://docs.moodle.org/dev/Web_service_API_functions
  https://docs.moodle.org/dev/Core_APIs
  """

  def __init__(self, token, privatetoken, host):
    self.token = token
    self.privatetoken = privatetoken
    self.host = host
    self.s = requests.Session()

  def request_ajax(self, reqs):
    """
    Takes a list of requests and their argument and returns responses to those in same order.
    Uses /lib/ajax/service.php endpoint.
    Can be used for unauthenticated requests.
    """
    raise NotImplementedError()

  @overload
  def request_ws(
    self,
    method: Literal['core_course_get_courses'],
  ) -> dict:
    ...

  @overload
  def request_ws(
    self,
    method: Literal['core_course_get_enrolled_courses_by_timeline_classification'],
    *,
    # https://github.com/moodle/moodle/blob/1d99ba19a21d57e9f1ed4211a8eeee00e50b7baf/course/externallib.php#L3749
    classification: Literal['future', 'inprogress', 'past']
  ) -> dict:
    ...

  @overload
  def request_ws(
    self,
    method: Literal['core_course_search_courses'],
    *,
    # https://github.com/moodle/moodle/blob/1d99ba19a21d57e9f1ed4211a8eeee00e50b7baf/course/externallib.php#L3749
    criterianame: Literal['search','modulelist','blocklist','tag','id'],
    criteriavalue: str  # The value the field must contain (for search a substring)
  ) -> dict:
    ...

  @overload
  def request_ws(
    self,
    method: Literal['core_webservice_get_site_info']
  ) -> SiteInfo: ...

  def request_ws(self, method, **args) -> dict:
    """
    Calls a method using Moodle WebService REST API.
    Can be used for authenticated requests.
    Uses /webservice/rest/server.php
    """
    res = requests.get(
      f'https://{self.host}/webservice/rest/server.php',
      params={
        'wstoken': self.token,
        'wsfunction': method,
        'moodlewsrestformat': 'json',
        **args
      }
    ).json()
    if 'exception' in res:
      print(res['exception'])
      raise MoodleException(**res)
    return res

  #def core_course_get_courses(self):
  #  return self.request_ws('core_course_get_courses')
#
  #def