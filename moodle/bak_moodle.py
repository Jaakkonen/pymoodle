import subprocess
import requests
from urllib.parse import urlparse, parse_qs
import dbm
from dataclasses import dataclass

@dataclass
class EnrolledCourse:
  id: int
  fullname: str
  shortname: str
  idnumber: str
  summary: str # HTML
  summaryformat: int
  startdate: int # unix timestamp
  enddate: int
  visible: bool
  fullnamedisplay: str
  viewurl: str
  courseimage: str
  progress: int
  hasprogress: bool
  isfavourite: bool
  hidden: bool
  showshortname: bool
  coursecategory: str


def moodle_rpc_ws(host, ):


class Moodle:
  """
  Moodle wrapper.
  """

  def __init__(self, host: str, token: str, privatetoken: str):
    self.token = token
    self.privatetoken = privatetoken
    self.host = host
    self.s = requests.Session()
    self.course = self.course(self)

    breakpoint()

  def get_recent_courses(self):
    raise NotImplementedError("MyCourses does not implement this for some reason")
    return self._request_ws('core_course_get_recent_courses')

  @dataclass
  class course:
    p: "Moodle"
    @property
    def categories(self):
      return self.p._request_ws('core_course_get_categories')

    def get_enrolled(self, status: str = 'inprogress'):
      return self.p._request_ws('core_course_get_enrolled_courses_by_timeline_classification', {
        'classification': status
      })


def main():
  with dbm.open('cache', 'c') as db:
    try:
      token = db['token'].decode()
      privatetoken = db['privatetoken'].decode()
      host = db['host'].decode()
    except KeyError:
      print("Tokens not found from cache. Getting those using QR from clipboard...")
      link = text_from_clipboard_qr()
      host, token, privatetoken = token_from_qr_link(link)
      db['token'] = token
      db['privatetoken'] = privatetoken
      db['host'] = host

  m = Moodle(host, token, privatetoken)
  #c = m.get_recent_courses()
  print(m.course.categories)
  breakpoint()

if __name__ == "__main__":
  main()

