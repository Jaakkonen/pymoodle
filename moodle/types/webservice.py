from typing import TypedDict


class SiteInfo(TypedDict):
  sitename: str
  username: str
  firstname: str
  lastname: str
  fullname: str
  lang: str
  userid: int
  siteurl: str
  userpictureurl: str
  functions: list['MoodleFunction']
  downloadfiles: int
  uploadfiles: int
  release: str
  version: str
  mobilecssurl: str
  advancedfeatures: list['AdvancedFeature']
  usercanmanageownfiles: bool
  userquota: int
  usermaxuploadfilesize: int
  userhomepage: int
  userprivateaccesskey: str
  siteid: int
  sitecalendartype: str
  usercalendartype: str
  userissiteadmin: bool
  theme: str

class MoodleFunction(TypedDict):
  name: str
  version: str

class AdvancedFeature(TypedDict):
  name: str
  value: int
