from typing import TypedDict


class EnrolledCoursesRes(TypedDict):
  courses: list["EnrolledCourse"]
  nextoffset: int

class EnrolledCourse(TypedDict):
  id: int
  fullname: str
  shortname: str
  idnumber: str
  summary: str
  summaryformat: int
  startdate: int
  enddate: int
  visible: bool
  fullnamedisplay: str
  viewurl: str
  courseimage: str
  progress: int
  hasprogress: bool
  isfavourite: bool
  hidden: bool
  showshortname: bool
  coursecategory: str
