from dataclasses import dataclass

@dataclass
class MoodleException(Exception):
  exception: str
  errorcode: str
  message: str


class Moodle:
  ...