import subprocess
from attr import dataclass
import requests
from urllib.parse import urlparse, parse_qs

def text_from_clipboard_qr() -> str:
  """
  Returns string got from decoding QR code in a image on clipboard.
  """
  return subprocess.run(
    "wl-paste | zbarimg --raw -q -",
    shell=True,
    capture_output=True
  ).stdout.strip().decode("utf-8")

def token_from_qr_link(qr_link: str) -> tuple[str, str, str]:
  """
  Takes in a moodle qr link like
  moodlemobile://https://mycourses.aalto.fi?qrlogin=a51034544ca0363d863af7dce3e1f32f&userid=45546
  parses it and tries retrieving tokens with it.
  Returns (host, token, pritavetoken) if successful.
  """

  url = qr_link

  url_data = urlparse(url.strip("moodlemobile://"))
  host = url_data.hostname
  assert host, "Hostname cannot be empty or None."
  query_data = parse_qs(url_data.query)

  res = requests.post(
    f"https://{host}/lib/ajax/service.php",
    headers={
      'user-agent': 'MoodleMobile'
    },
    json=[{
      'index': 0,
      'methodname': 'tool_mobile_get_tokens_for_qr_login',
      'args': {
        'qrloginkey': query_data['qrlogin'][0],
        'userid': query_data['userid'][0]
      }
    }]
  ).json()
  assert res[0]['error'] is False, "Request to get tokens from QR failed"

  return host, res[0]['data']['token'], res[0]['data']['privatetoken']

@dataclass
class MoodleCredentials:
  host: str
  token: str
  privatetoken: str
