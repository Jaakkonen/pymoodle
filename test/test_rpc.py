from datetime import timedelta
from moodle.rpc import MoodleException, MoodleRPC
import pytest
def test_get_courses_not_allowed_if_not_enrolled(rpc: MoodleRPC):
  with pytest.raises(MoodleException) as e:
    res = rpc.request_ws('core_course_get_courses')
  # assert e.value.message == 'Sorry, but you do not currently have permissions to do that (View courses without participation).'
  assert e.value.message == 'You cannot execute functions in the course context (course id:5473). The context error message was: Course or activity not accessible.'


def test_list_enrolled(rpc: MoodleRPC):
  res = rpc.request_ws(
    'core_course_get_enrolled_courses_by_timeline_classification',
    classification='inprogress'
  )


def test_search(rpc: MoodleRPC):
  res = rpc.request_ws(
    'core_course_search_courses',
    criterianame='search',
    criteriavalue='asd'
  )


@pytest.mark.skip("Timeouts with 502")
def test_courses_by_field(rpc: MoodleRPC):
  res = rpc.request_ws(
    'core_course_get_courses_by_field',
  )


def test_courses_by_cmid(rpc: MoodleRPC):
  with pytest.raises(MoodleException) as e:
    res = rpc.request_ws(
      'core_course_get_enrolled_users_by_cmid'
    )
  assert e.value.message == 'Access control exception'


def test_list_functions(rpc: MoodleRPC):
  res = rpc.request_ws('core_webservice_get_site_info')
  # TODO: Add assertions

def test_get_assignments(rpc: MoodleRPC):
  res = rpc.request_ws('mod_assign_get_assignments')
  #breakpoint()

def test_submit_dummy(rpc: MoodleRPC):
  from datetime import datetime
  res = rpc.request_ws('mod_assign_get_assignments')
  for course in res['courses']:
    for assignment in course['assignments']:
      if datetime.now() - datetime.fromtimestamp(assignment['duedate']) < timedelta(hours=48):
        breakpoint()
