import dbm
import pytest
from moodle.login import MoodleCredentials, text_from_clipboard_qr, token_from_qr_link
from moodle.rpc import MoodleRPC

@pytest.fixture()
def rpc(credentials: MoodleCredentials) -> MoodleRPC:
  return MoodleRPC(
    credentials.token,
    credentials.privatetoken,
    credentials.host
  )

@pytest.fixture()
def credentials() -> MoodleCredentials:
  with dbm.open('cache', 'c') as db:
    try:
      token = db['token'].decode()
      privatetoken = db['privatetoken'].decode()
      host = db['host'].decode()
    except KeyError:
      print("Tokens not found from cache. Getting those using QR from clipboard...")
      input("Please copy QR to clipboard and press Enter...")
      link = text_from_clipboard_qr()
      host, token, privatetoken = token_from_qr_link(link)
      db['token'] = token
      db['privatetoken'] = privatetoken
      db['host'] = host

  return MoodleCredentials(
    host, token, privatetoken
  )