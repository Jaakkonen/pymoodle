from typing import Any, Optional, Type
from util.phply.phply import phplex
from util.phply.phply.phpparse import make_parser
from util.phply.phply import phpast as ast
from collections import defaultdict

parser = make_parser()
def get_ast(input: str, filename=None) -> list[ast.Node]:
  output = parser.parse(input, lexer=phplex.lexer)
  return output

def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

def parse_external_value(call: ast.New) -> tuple[Type, str, Optional[bool], Optional[Any]]:
  """
  Returns:
    type
    description
    is required
    default value
  """
  assert isinstance(call, ast.New)
  assert call.name == 'external_value'
  params: list[ast.Parameter] = call.params
  assert len(params) in {2, 3, 4}
  typ = params[0]
  assert isinstance(typ, ast.Parameter)
  if isinstance(typ.node, ast.Constant):
    typ_str = typ.node.name
    if typ_str == 'PARAM_INT':
      typ = int
    else:
      breakpoint()
  desc = params[1]
  default = None
  try:
    required = params[2]
    assert isinstance(required.node, ast.Constant)
    if required.node.name == 'VALUE_REQUIRED':
      required = True
    elif required.node.name == 'VALUE_DEFAULT':
      required = False
      default = params[3].node
    else:
      breakpoint()
  except IndexError:
    required = None

  return (typ, desc.node, required, default)


def parse_ext_multiple_structure(call: ast.New) -> tuple[tuple[Type, str], str, bool]:
  """
  Represents list of multiple items of some type.
  Returns:
    type of variable
    description
    is required
  """
  assert isinstance(call, ast.New)
  assert call.name == 'external_multiple_structure'
  params = call.params
  assert len(params) == 3
  typ = params[0]
  desc = params[1]
  required = params[2]
  if isinstance(typ.node, ast.New) and typ.node.name == 'external_value':
    typ = parse_external_value(typ.node)
  if isinstance(required.node, ast.Constant):
    if required.node.name == 'VALUE_REQUIRED':
      required = True
    else:
      breakpoint()
  else:
    breakpoint()
  return (typ, desc.node, required)

def parse_single_structure(call: ast.New):
  ...


def resolve_parameters(fun: ast.Method, priv_funs: dict[str, ast.Method]) -> dict[str, tuple[Any, str, bool, Optional[Any]]]:
  assert isinstance(fun, ast.Method)
  assert len(fun.nodes) == 1
  ret: ast.Return = fun.nodes[0]
  assert isinstance(ret, ast.Return)
  assert isinstance(ret.node, ast.New)
  assert len(ret.node.params) == 1
  assert len(ret.node.params) == 1
  assert isinstance(ret.node.params[0].node, ast.Array)
  ret_arr: list[ast.ArrayElement] = ret.node.params[0].node.nodes
  ret_values: dict[str, tuple[Any, str, bool, Optional[Any]]] = {}
  for ret_attr in ret_arr:
    key: str = ret_attr.key
    value = ret_attr.value
    if isinstance(value, ast.New):
      if value.name == 'external_multiple_structure':
        value = parse_ext_multiple_structure(value)
      elif value.name == 'external_value':
        value = parse_external_value(value)
      else:
        breakpoint()
    else:
      breakpoint()
    ret_values[key] = value
  return ret_values

def resolve_returns(fun: ast.Method, priv_funs: dict[str, ast.Method]):
  info = {}
  assert isinstance(fun, ast.Method)
  assert len(fun.nodes) == 1
  ret: ast.Return = fun.nodes[0]
  assert isinstance(ret, ast.Return)
  assert isinstance(ret.node, ast.New)
  if ret.node.name == 'external_single_structure':
    info['multiple'] = False
  elif ret.node.name == 'external_multiple_structure':
    info['multiple'] = True
  else:
    raise ValueError()

  value =
  assert len(ret.node.params) == 1
  assert len(ret.node.params) == 1
  assert isinstance(ret.node.params[0].node, ast.Array)
  ret_arr: list[ast.ArrayElement] = ret.node.params[0].node.nodes
  ret_values: dict[str, tuple[Any, str, bool, Optional[Any]]] = {}
  for ret_attr in ret_arr:
    key: str = ret_attr.key
    value = ret_attr.value
    if isinstance(value, ast.New):
      if value.name == 'external_multiple_structure':
        value = parse_ext_multiple_structure(value)
      elif value.name == 'external_value':
        value = parse_external_value(value)
      else:
        breakpoint()
    else:
      breakpoint()
    ret_values[key] = value
  return ret_values


def parse_module_extlib(filename: str):
  with open(filename) as f:
    syms = get_ast(f.read())
  cls: ast.Class = next(sym for sym in syms if isinstance(sym, ast.Class))
  assert cls.extends == '\\mod_assign\\external\\external_api'
  module: str = cls.name
  assert module.startswith('mod_')
  assert module.endswith('_external')
  nodes: list[ast.Method | ast.Comment] = cls.nodes
  assert all(isinstance(node, (ast.Method, ast.Comment)) for node in nodes)

  pub_funcs = defaultdict(dict)
  priv_funcs = {}
  for comment, method in chunker(nodes, 2):
    if 'private' in method.modifiers:
      priv_funcs[method.name] = method
      continue
    assert isinstance(comment, ast.Comment)
    assert isinstance(method, ast.Method)

    name: str = method.name
    if name.endswith('_parameters'):
      pub_funcs[name.removesuffix('_parameters')]['params'] = method
      pub_funcs[name.removesuffix('_parameters')]['params_comment'] = comment.text
    elif name.endswith('_returns'):
      pub_funcs[name.removesuffix('_returns')]['ret'] = method
      pub_funcs[name.removesuffix('_returns')]['ret_comment'] = comment.text
    else:
      pub_funcs[name]['fun'] = method
      pub_funcs[name]['comment'] = comment.text

  for func, props in pub_funcs.items():
    parameters = resolve_parameters(props['params'], priv_funcs)
    returns = resolve_returns(props['ret'], priv_funcs)

parse_module_extlib('debugging/moodle/mod/assign/externallib.php')
