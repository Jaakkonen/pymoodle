<?php
require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/webservice/lib.php');

admin_externalpage_setup('webservicedocumentation');

$functions = $DB->get_records('external_functions', array(), 'name');
$functiondescs = array();
foreach ($functions as $function) {
    $functiondescs[$function->name] = external_api::external_function_info($function);
}

function walk_descs(&$obj) {
	if ($obj instanceof external_value) {
		if ($obj->default instanceof \Closure) {
			$obj->default = $obj->default->call();
		}
	}
	if ($obj instanceof external_single_structure) {
		foreach ($obj as $key => &$val){
			walk_descs($val);
		}
	}
	if (is_array($obj)) {
		foreach ($obj as &$val){
			walk_descs($val);
		}
	}
}
foreach($functiondescs as $name => &$desc){
	walk_descs($desc->parameters_desc->keys);
	walk_descs($desc->returns_desc->keys);
}

echo serialize($functiondescs);
