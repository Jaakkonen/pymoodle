from os import environ
import docker
from docker.client import DockerClient
from docker.models.containers import Container
import time
from pathlib import Path
client: DockerClient = docker.from_env()
pg_container = client.containers.run(
  "docker.io/postgres:latest",
  remove=True,
  detach=True,
  environment={
    'POSTGRES_PASSWORD': 'example'
  },
  network_mode='host'
)
moodle_container = client.containers.run(
  "docker.io/bitnami/moodle:3",
  remove=True,
  detach=True,
  environment={
    'MOODLE_DATABASE_HOST': 'localhost',
    'MOODLE_DATABASE_PORT_NUMBER': '5432',
    'MOODLE_DATABASE_TYPE': 'pgsql',
    'MOODLE_DATABASE_USER': 'postgres',
    'MOODLE_DATABASE_NAME': 'postgres',
    'ALLOW_EMPTY_PASSWORD': 'yes',
    'BITNAMI_DEBUG': 'true',
    'POSTGRESQL_CLIENT_POSTGRES_PASSWORD': 'example'
  },
  network_mode='host',
)
assert isinstance(pg_container, Container) and isinstance(moodle_container, Container)

for line in moodle_container.logs(stream=True, follow=True):
  if '** Starting Apache **' in line.decode('utf-8'):
    break
time.sleep(1)

import io
import tarfile
import os
def copy_to_container(container: Container, src: str, dst_dir: str):
    """ src shall be an absolute path """
    stream = io.BytesIO()
    with tarfile.open(fileobj=stream, mode='w|') as tar, open(src, 'rb') as f:
        info = tar.gettarinfo(fileobj=f)
        info.name = os.path.basename(src)
        tar.addfile(info, f)

    container.put_archive(dst_dir, stream.getvalue())

src = str(Path(__file__).absolute().parent / 'dump_funcdescs.php')
copy_to_container(moodle_container, src, '/bitnami/moodle/admin/webservice')
moodle_container.exec_run(['chown', 'daemon:daemon', '/bitnami/moodle/admin/webservice/dump_funcdescs.php'])

import requests
from bs4 import BeautifulSoup

s = requests.Session()
login = s.get('http://localhost:8080/login/index.php')
login_bs = BeautifulSoup(login.text, features='lxml')
login_token: str = login_bs.find('input', {'name':'logintoken'})['value']
log_post = s.post('http://localhost:8080/login/index.php', data={
  'anchor': '',
  'logintoken': login_token,
  'username': 'user',
  'password': 'bitnami'
})
functiondescs_serialized = s.get('http://localhost:8080/admin/webservice/dump_funcdescs.php').text

(Path(__file__).parent / 'function_descs_serialized.ser').open('w').write(functiondescs_serialized)

breakpoint()