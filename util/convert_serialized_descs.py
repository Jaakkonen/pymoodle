from __future__ import annotations
from pathlib import Path
from typing import Any, Literal, Optional, Union
from myphpserialize import unserialize
from pydantic.dataclasses import dataclass
from pydantic import BaseModel
import json
functiondescs_serialized = (Path(__file__).parent / 'function_descs_serialized.ser').open('rb').read()

def object_hook(name, vals):
  return {
    "__phptype": name,
    **vals
  }

class Value(BaseModel):
  __phptype: Literal['external_value']
  type: str
  allownull: bool | int = False
  desc: str
  required: int
  default: Any

class Single(BaseModel):
  __phptype: Literal['external_single_structure']
  keys: dict[str, Value | Single | Multiple] = {}
  @property
  def openapi_properties(self):
    ...


class Multiple(BaseModel):
  __phptype: Literal['external_multiple_structure']
  content: Value | Single
  desc: str
  required: int | dict
  default: Any
  @property
  def openapi_properties(self):
    ...

Single.update_forward_refs()


class Argument(BaseModel):
  __root__: Value | Single | Multiple
  @property
  def openapi_properties(self):
    ...

class SingleParameter(BaseModel):
  method: Value
  component: Value
  args: Argument

  @property
  def openapi_properties(self):
    ...

class Parameters(BaseModel):
  __phptype: Literal['external_function_parameters']
  keys: dict[str, Argument] | SingleParameter | None

  @property
  def openapi_properties(self):
    ...

class Method(BaseModel):
  __phptype: Literal['stdClass']
  id: int
  name: str
  services: Optional[str] = None
  allowed_from_ajax: bool
  parameters_desc: Parameters
  returns_desc: Parameters
  description: str
  loginrequired: bool
  readonlysession: bool
  type: Literal["read", "write"]


class Methods(BaseModel):
  __root__: dict[str, Method]

functiondescs = unserialize(functiondescs_serialized, object_hook=object_hook, decode_strings=True)
with open('functiondescs.json', 'w') as f:
  json.dump(functiondescs, f, indent=2)
methods = Methods(__root__=functiondescs)

with open('functiondescs_filtered.json', 'w') as f:
  f.write(methods.json())
